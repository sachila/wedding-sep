<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class signup extends CI_Controller {

    public function index() {
        
    }

    public function customer() {
        $this->load->model('signupmodel');
        $this->signupmodel->customersignup();
        $this->load->view('navbar');
        $this->load->view('signupact');
        $this->load->view('footer');
    }

    public function sprovider() {
        $name = $this->input->post('spname');
        $email = $this->input->post('spemail');
        $password = $this->input->post('firstpass');
        $mob = $this->input->post('spmob');

        $sppack = $this->input->post('all');
        $web = $this->input->post('spweb');
        $fb = $this->input->post('spfb');
        $tw = $this->input->post('sptw');
        $youtube = $this->input->post('spytb');
 $spty = $this->input->post('sptype');

        $str2 = 'free';
        if ($sppack != $str2) {

            $this->load->view('navbar');
            $dat = array(
                'email' => $email,
                'name'=>$name,
                'password'=>$password,
                'mob'=>$mob,'web'=>$web,
                'fb'=>$fb,'tw'=>$tw,
                'youtube'=>$youtube,
                'sppack'=>$sppack,
                'sptype'=>$spty
                    );
            $this->load->view('spsignuppay', $dat);
           
            $this->load->view('footer');
        } else {
            $this->load->model('signupmodel');
            $this->signupmodel->spsignup($name,$email,$password,$mob,$sppack,$web,$fb,$tw,$youtube,$spty);
            $this->load->view('navbar');
            $this->load->view('signupact');
            $this->load->view('footer');
        }
    }

    public function sppay() {
        $name = $this->input->post('hel1');
        $email = $this->input->post('hell');
        $password = $this->input->post('hel3');
        $mob = $this->input->post('hel2');

        $sppack = $this->input->post('hel8');
        $web = $this->input->post('hel4');
        $fb = $this->input->post('hel5');
        $tw = $this->input->post('hel6');
        $youtube = $this->input->post('hel7');
        $this->load->model('signupmodel');
        $this->signupmodel->spsignup($name,$email,$password,$mob,$sppack,$web,$fb,$tw,$youtube);
        $this->load->model('signupmodel');
        $this->signupmodel->spsignuppay($email);
        $this->load->view('navbar');
        $this->load->view('signupact');
        $this->load->view('footer');
    }

}