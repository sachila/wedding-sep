<?php
 

defined('BASEPATH') OR exit('No direct script access allowed');

class packages extends my_controller {
    //add new meal package 
    public function index(){        
       $this->package_index();     
     }
    //get meal details to addition page 
    public function user_additional_details($meal,$user){
        $this->user_additional_details_index($meal,$user);
     }

    //add new meal package to user account according to id 
    public function user_newpackage($id){        
        $this->user_new_package($id);
        
     }
    //get all additional details 
    public function additional_details(){
         $this->addtional_details_index();
     }
    //ad new additional details to db by admin
    public function add_additional_details(){
         $this->new_additional_details();
     }
    //view all meal packages 
    public function allpackages(){
        $this->all_package();
        
    }
    // add new meal package
    public function newpackage(){
        $this->new_package();        
    }
    //get user selected package 
    public function exsistingpackages(){
        $this->exsisting_package();
    }
    //validatio that allow user to add only one meal package to one profile    
    public function user_addpackage(){        
        $this->user_add_package();
    }
    //edit user selectes meal package
    public function user_editpackage($id){
        $this->user_edit_package($id);
    }
    // save edited user packages 
    public function edit_userpackage(){
        $this->edit_user_package();
    }
    
    //save additional details enter by the user
    public function additional_create(){
        // get meal name and user name 
        $ptype = $this->input->post('meal');
        $user = $this->input->post('user');
        
        $dataarray = array(); //get all additional details by id to this array
        // get all checkbox values to $data 
        $data= $this->input->post('additionaldata');
        $this->load->model('package');
           
        $total = 0; // total amount of the package
        $des = ''; //additional details name and price
        if ($data) {
            
        
         foreach ($data as $data => $k) {
                //get all additional details by id to this array
                $dataarray['result'] =  $this->package->get_additional_detail_byid($k);
                for($i=0;$i<count($dataarray['result']);$i++){
                       $des .= $dataarray['result'][$i]->title.' - '.$dataarray['result'][$i]->price.' , ';
                       $total = intval($total) + intval($dataarray['result'][$i]->price);
                    }                     
         }
         }
         try {
             $this->package->user_additional_info($ptype,$user,$des,$total);        
             
         } catch (Exception $exc) {
             echo $exc->getTraceAsString();
             echo '<script>alert("Failure Occure While saving Data");</script>';
     
         }         
         // send email
         try {
             
         $config = Array(
             'protocol' => 'smtp',
             'smtp_host' => 'ssl://smtp.googlemail.com',
             'smtp_port' => 465,
             'smtp_user' => 'sachila@duosoftware.com',
             'smtp_pass' => 'sachila@33',
             'mailtype'  => 'html', 
             'charset'   => 'iso-8859-1'
         );
         $this->load->library('email', $config);
         $this->email->set_newline("\r\n");
         $this->email->from('sachila@duosoftware.com', 'Sachila Ranawaka');
         $this->email->to('sachilaranawaka@gmail.com'); 

         //get package by name to $mailarr
         $mailarr = array();
         $mailarr['result'] = $this->package->get_exsisting_packages_byname($ptype);

         for($i=0;$i<count($mailarr['result']);$i++){
             //mail subject and body
             $this->email->subject('MY Wedding.lk');
             $this->email->message(''.$ptype.'\
             '.$ptype.' MENU\
             Person :'.$mailarr['result'][$i]->person.'\
             Date :'.$mailarr['result'][$i]->Date.'\
             User :'.$mailarr['result'][$i]->user.'\
             Payment :'.$mailarr['result'][$i]->paymenttype.'\
             Start Date :'.$mailarr['result'][$i]->startdate.'\
             End Date :'.$mailarr['result'][$i]->enddate.'\
             Description :'.$mailarr['result'][$i]->description.'\
             Location :'.$mailarr['result'][$i]->location.'\
             Charge :'.$mailarr['result'][$i]->Charge.'\
             RS '.$mailarr['result'][$i]->person.'.00/- PER PERSON - till '.$mailarr['result'][$i]->enddate.'\
             Subject to '.$mailarr['result'][$i]->Charge.'% service charge and applicable government taxes\
             Total:'.$total.'.00/=\
             Additional Details\
             '.$des.'');	
         }

             $result = $this->email->send();
             //if mail send 
             if($result){
               echo '<script>alert("Package added to your profile, Please check your email");</script>';
               echo $this->email->print_debugger();
               $this->exsisting_package();
             }
             
         } catch (Exception $exc) {
             echo $exc->getTraceAsString();
             echo '<script>alert("Error occure while sending the email");</script>';
              
         }   
        
    }
    // add new additional details by admin to database 
    public function addtional_create(){
                 // validate form data
                 $this->load->library('form_validation');
                 $this->form_validation->set_error_delimiters('<div class="error">', '</div>'); 
                 $this->form_validation->set_rules('title', 'Title', 'required');
                 $this->form_validation->set_rules('price', 'Price', 'required');	
                 $this->form_validation->set_rules('description', 'Description', 'required');	
                 
                 if ($this->form_validation->run() == FALSE) {
                       $this->new_additional_details();
                 }  else {
                        //get all user enter foem data
                        $title= $this->input->post('title');           
                        $price= $this->input->post('price');
                        $description= $this->input->post('description');                       
                        
                        //upload images 
                        $this->load->library('upload');
                        $this->load->model('galarydb');
                        $config['upload_path'] = './Assets/Additional/';
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size']	= '1000000';
                        $config['max_width']  = '10240';
                        $config['max_height']  = '7680';
                        $this->load->library('upload', $config);
                        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
                        $this->upload->initialize($config);
                        if( ! $this->upload->do_upload()){
                            echo '<script language="javascript">';
                                echo 'alert("Upload failed! Please check the file size and file format")';
                                echo '</script>';
                                $this->new_additional_details();
                        }  else {
                            $this->upload->data();
                            $path = './Assets/Additional/'; //path of the file 
                            $file_name = $this->upload->file_name; //get upload file name
                            $this->load->model('package');
                            try {
                                //add new additional details
                                $this->package->add_addtional_details($file_name,$path,$title,$description,$price);    
                                $this->addtional_details_index();   
                                echo '<script language="javascript">';
                                echo 'alert("Data successfuly Saved")';
                                echo '</script>';
                                
                            } catch (Exception $exc) {
                                echo $exc->getTraceAsString();
                                $this->addtional_details_index();   
                                echo '<script language="javascript">';
                                echo 'alert("Failure Occure While saving Data")';
                                echo '</script>';
                            }                            
                        }
                 }                
    }

    //add new meal package by admin
    public function add_userpackage(){
             // validate form data
                 $this->load->library('form_validation');
                 $this->form_validation->set_error_delimiters('<div class="error">', '</div>'); 
                 $this->form_validation->set_rules('systemUser', 'Payment type', 'required');
                 $this->form_validation->set_rules('startdate', 'Start Date', 'required');	
                 $this->form_validation->set_rules('enddate', 'End Date', 'required');	
                 $this->form_validation->set_rules('description', 'Description', 'required');	
                 $this->form_validation->set_rules('location', 'Location', 'required');	
                 $id = $this->input->post('packageid');

             if ($this->form_validation->run() == FALSE){
                 
               $this->user_new_package($id);
	
               }
		else{
                //get all form data
                 $ptype = $this->input->post('ptype');           
                 $location = $this->input->post('location');
                 $description = $this->input->post('description');
                 $enddate = $this->input->post('enddate');
                 $startdate = $this->input->post('startdate');
                 $systemUser = $this->input->post('systemUser');
                 $user = $this->input->post('user');
                 $Charge = $this->input->post('Charge');
                 $date = $this->input->post('Date');
                 $person = $this->input->post('person');
                 $this->load->model('package');
                 
                 try {
                   $this->package->add_userpackage($ptype, $person, $date, $user, $systemUser, $startdate,$enddate,$description,$location,$Charge);
//                 echo '<script>alert("Successfully Inserted");</script>';
                   $this->user_additional_details($ptype,$user);
                 } catch (Exception $exc) {
                     echo $exc->getTraceAsString();
                     echo '<script>alert("Error Occure");</script>';
               
                 }
             } 
     }

        public function create(){ 
             // validate form data
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>'); 
            $this->form_validation->set_rules('ptype', 'Type', 'required|min_length[5]');
            $this->form_validation->set_rules('soups', 'Soups', 'required');
            $this->form_validation->set_rules('appetizers', 'Appetizers', 'required');
            $this->form_validation->set_rules('salads', 'Salads', 'required');
            $this->form_validation->set_rules('meat', 'Meat', 'required');
            $this->form_validation->set_rules('fish', 'Fish', 'required');	
            $this->form_validation->set_rules('vegetable', 'Vegetable', 'required');
            $this->form_validation->set_rules('rice', 'Rice', 'required');
            $this->form_validation->set_rules('person', 'Person', 'required');
            $this->form_validation->set_rules('Date', 'Due Date', 'required');	
            $this->form_validation->set_rules('Charge', 'Service Charge', 'required');	
            $this->form_validation->set_rules('desserts', 'desserts', 'required');	

        
           if ($this->form_validation->run() == FALSE)
		{
               $this->new_package();
		}
		else{                
                 $ptype = $this->input->post('ptype');
                 $soups = $this->input->post('soups');
                 $appetizers = $this->input->post('appetizers');
                 $salads = $this->input->post('salads');
                 $meat = $this->input->post('meat');
                 $fish = $this->input->post('fish');
                 $vegetable = $this->input->post('vegetable');
                 $rice = $this->input->post('rice');
                 $person = $this->input->post('person');
                 $date = $this->input->post('Date');
                 $Charge = $this->input->post('Charge');
                 $desserts = $this->input->post('desserts');
                 $this->load->model('package');
                 
                 try {
                            $this->package->add_package($ptype, $soups, $appetizers, $salads, $meat, $fish,$vegetable,$rice,$person,$date,$Charge,$desserts);
                            echo '<script>alert("Successfully Inserted");</script>';
                            $this->package_index();
                     
                 } catch (Exception $exc) {
                        echo $exc->getTraceAsString();
                        echo '<script>alert("Error Occure");</script>';
                           
                 }
         }
    }
    
    //update meal package by id 
    public function updateid($id){        
        $this->load->model('package');
        $this->load->helper('url');
        //get meal package according to id 
        $data['result'] = $this->package->get_package_byid($id);
        $this->load->view('navbar');
        $this->load->view('update_package',$data);
        $this->load->view('footer'); 
    }
    
    public function update(){        

            // validate form data       
           $this->load->library('form_validation');
           $this->form_validation->set_error_delimiters('<div class="error">', '</div>'); 
           $this->form_validation->set_rules('ptype', 'Type', 'required|min_length[5]');
           $this->form_validation->set_rules('soups', 'Soups', 'required');
           $this->form_validation->set_rules('appetizers', 'Appetizers', 'required');
           $this->form_validation->set_rules('salads', 'Salads', 'required');
           $this->form_validation->set_rules('meat', 'Meat', 'required');
           $this->form_validation->set_rules('fish', 'Fish', 'required');	
           $this->form_validation->set_rules('vegetable', 'Vegetable', 'required');
           $this->form_validation->set_rules('rice', 'Rice', 'required');
           $this->form_validation->set_rules('person', 'Person', 'required');
           $this->form_validation->set_rules('Date', 'Due Date', 'required');	
           $this->form_validation->set_rules('Charge', 'Service Charge', 'required');	
           $this->form_validation->set_rules('desserts', 'desserts', 'required');	

        
           if ($this->form_validation->run() == FALSE)
		{
               $this->new_package();
		}
		else{
                
                 $ptype = $this->input->post('ptype');
                 $soups = $this->input->post('soups');
                 $appetizers = $this->input->post('appetizers');
                 $salads = $this->input->post('salads');
                 $meat = $this->input->post('meat');
                 $fish = $this->input->post('fish');
                 $vegetable = $this->input->post('vegetable');
                 $rice = $this->input->post('rice');
                 $person = $this->input->post('person');
                 $date = $this->input->post('Date');
                 $Charge = $this->input->post('Charge');
                 $desserts = $this->input->post('desserts');
                 $id = $this->input->post('id');
                 $this->load->model('package');
                 
                 try {
                 $this->package->update_package($id,$ptype, $soups, $appetizers, $salads, $meat, $fish,$vegetable,$rice,$person,$date,$Charge,$desserts);
                 echo '<script>alert("Successfully Updated");</script>';
                 $this->package_index();

                 } catch (Exception $exc) {
                     echo $exc->getTraceAsString();
                      echo '<script>alert("Error Occure);</script>';
                 
                 }
           }
        
    }
    
    //remove meal package
    public function remove_category(){
     //get selected check boxes
     $checked = $this->input->post('msg');
     $this->load->model('package');
     try {
        foreach ($checked as $checked => $k) { //get selectbox value 
            $this->package->delete_category($k);  //deleta meal package 
        }
        $this->package_index();
         echo '<script>alert("Successfully Deleted");</script>';   

     } catch (Exception $exc) {
         echo $exc->getTraceAsString();
         echo '<script>alert("Error Occure while Deleting");</script>';   

     }

   }
    //remove addirional details 
    public function remove_additional_details(){
     //get selected check boxes
     $checked = $this->input->post('msg');
     $this->load->model('package');
     try {
          foreach ($checked as $checked => $k) {//get selectbox value 
            $this->package->delete_addional_details($k); //delete additional details
           }
            $this->additional_details();
            echo '<script>alert("Successfully Deleted");</script>';   

     } catch (Exception $exc) {
         echo $exc->getTraceAsString();
          echo '<script>alert("Error Occure while Deleting");</script>';   

     }   
    }
    //search additional details
    public function search_category(){        
        $this->search_location();
    }
    //search additional details
    public function addition_search(){
        $this->addition_search_new();
    }
    //remove meal package from profile
    public function remove_user_newpackage($id){ //user id getting from login 
         $this->load->model('package');
         //delete user category
         $this->package->delete_user_category($id);         
         echo '<script>alert("Successfully Deleted");</script>'  ; 
         $this->all_package();
    }
}
 
?>
