<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->load->helper('url');
        $this->load->view('navbar');
        $this->load->view('homeup');
        $this->load->view('footer');
    }

    public function signup() {
        $this->load->helper('url');

        $this->load->view('navbar');
        $this->load->view('signupmain');
        $this->load->view('footer');
    }

    public function spsignup() {
        $this->load->helper('url');

        $this->load->view('navbar');
        $this->load->view('serviceprovidersignup');
        $this->load->view('footer');
    }

    public function csignup() {
        $this->load->helper('url');

        $this->load->view('navbar');
        $this->load->view('customersignup');
        $this->load->view('footer');
    }

    public function packinfo() {
        $this->load->helper('url');

        $this->load->view('navbar');
        $this->load->view('spsignuppack');
        $this->load->view('footer');
    }

      public function sppay() {
        $this->load->helper('url');

        $this->load->view('navbar');
        $this->load->view('spsignuppay');
        $this->load->view('footer');
    }

    public function wedplannervs() {
        $this->load->helper('url');

        $this->load->view('navbar');
        $this->load->view('viscustwpmain');
        $this->load->view('footer');
    }

    public function wedplanner() {
        $this->load->helper('url');

        $this->load->view('navbar');
        $this->load->view('visitorplanner');
        $this->load->view('footer');
    }

    public function contactus() {
        $this->load->helper('url');
        $this->load->view('navbar');
        $this->load->view('contactus');
        $this->load->view('footer');
    }

    public function termsandcond() {
        $this->load->helper('url');
        $this->load->view('navbar');
        $this->load->view('termscond');
        $this->load->view('footer');
    }

    public function guestbook() {
        $this->load->helper('url');
        $this->load->view('navbar');
        $this->load->model('guestmodel');
        $data['result'] = $this->guestmodel->getcommnts();
        $this->load->view('guestbook', $data);
        $this->load->view('footer');
    }

    public function ourmission() {
        $this->load->helper('url');
        $this->load->view('navbar');
        $this->load->view('ourmission');
        $this->load->view('footer');
    }

}
