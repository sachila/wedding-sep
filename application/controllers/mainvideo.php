<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mainvideo extends my_controller {

    // redirect to main galary page
    public function index() {    
    
        $this->load->helper('url');
        $this->load->view('navbar');
        $this->load->view('videomain');   
        $this->load->view('footer');
}
    // redirect to  profile page
    public function viewprofile() {    
    
        $this->load->helper('url');
        $this->load->view('navbar');
        $this->load->view('profile');   
        $this->load->view('footer');
}
    // redirect to  add new video page
    public function newvideo(){
         $this->load->helper('url');
        $this->load->view('navbar');
        $this->load->view('newvideo');   
        $this->load->view('footer');
    }
    // redirect to image load page
    public function uploadimages() {
        $this->uploadimages_con();
    }
    // redirect to load one video page
    public function setting($id){
        $this->setting_video($id);
    }
    //image upload function 
    public function doupload() {                   
      
    // user name getting from session 
    $id = "SAS";

    $name_array = array();
    // get upload file into $count
    $count = count($_FILES['userfile']['size']);
    foreach($_FILES as $key=>$value)
    for($s=0; $s<=$count-1; $s++) {
    $_FILES['userfile']['name']=$value['name'][$s];
    $_FILES['userfile']['type']    = $value['type'][$s];
    $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
    $_FILES['userfile']['error']       = $value['error'][$s];
    $_FILES['userfile']['size']    = $value['size'][$s];  

    $config['upload_path'] = './Assets/uploads/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size']	= '10000000';
    $config['max_width']  = '102400';
    $config['max_height']  = '76800';
    $this->load->library('upload', $config);
    $this->upload->initialize($config);

    $this->upload->do_upload();
    $data = $this->upload->data();
    $name_array[] = $data['file_name'];
    }


    $names= implode(',', $name_array);
    
    $path = './Assets/uploads/';
    //If the upload success

    if($this->upload->do_upload()){
        try {
            //get file name
            $file_name = $this->upload->file_name;
            $this->load->model('galarydb');
            //save upload image details into table 
            $this->galarydb->add_attachment($file_name,$id,$path);
            // if upload successfull redirect to image load page
            $this->uploadimages_con($id);
            echo '<script language="javascript">';
            echo 'alert("Upload successful")';
            echo '</script>';
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            echo '<script language="javascript">';
            echo 'alert("Upload failed! Please check the file size and file format")';
            echo '</script>';
        }
     }
     else if( ! $this->upload->do_upload()){
            echo '<script language="javascript">';
            echo 'alert("Upload failed! Please check the file size and file format")';
            echo '</script>';
            $this->uploadimages_con($id);
    }
    

    }
    //redirect to video load page
    public function loadvideo(){
          $this->loadvideo_con();
    }
    
    //video upload function 
    public function uploadvideo() {
         //check the validation of the fields 
         $this->load->library('form_validation');
         $this->form_validation->set_error_delimiters('<div class="error">', '</div>'); 
         $this->form_validation->set_rules('Category', 'Category', 'required');
         $this->form_validation->set_rules('Description', 'Description', 'required');	
               
        if ($this->form_validation->run() == FALSE) {            
            //if validation fails 
            $this->load->helper('url');
            $this->load->view('navbar');
            $this->load->view('newvideo');   
            $this->load->view('footer');          
       
        }  else { 
        
          // Category Details and Description details
          $Category= $this->input->post('Category');           
          $Description= $this->input->post('Description');
          //username getting from login 
          $id = "SAS";
          $this->load->library('upload');
          $this->load->model('galarydb');
          $config['upload_path'] = './Assets/videos/';
          $config['allowed_types'] = 'mp4|3gp|flv';
          $config['max_size'] = '500000';
          $this->load->library('upload', $config);
          
          // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
          $this->upload->initialize($config);
          //if upload fails
          
     if( ! $this->upload->do_upload()){
            echo '<script language="javascript">';
            echo 'alert("Upload failed! Please check the file size and file format")';
            echo '</script>';
            $this->loadvideo_con();
      }
          //If the upload success       

     else{
            $this->upload->data();
            $path = './Assets/videos/';
         try {
            //get file name
            $file_name = $this->upload->file_name;
            $this->load->model('galarydb');
            //save upload image details into table 
            $this->galarydb->add_videos($file_name,$id,$path,$Category,$Description);    
            //load video page
            $this->loadvideo_con();   
            echo '<script language="javascript">';
            echo 'alert("Upload successful")';
            echo '</script>';
             
         } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            echo '<script language="javascript">';
            echo 'alert("Upload failed! Please check the file size and file format")';
            echo '</script>';
            $this->loadvideo_con();
         }          
     }
   }
 }
    //delete images function 
    public function deleteimage($id){
        
            $this->load->model('galarydb');
            $this->load->helper('url');
            //database remove function 
            $this->galarydb->remove_image($id);
            $this->uploadimages_con();
            echo '<script>alert("Successfully Deleted");</script>'  ; 

    }
    //delete video function 
     public function deletevideo($id){
         
            $this->load->model('galarydb');
            $this->load->helper('url');
            //database remove function 
            $this->galarydb->remove_video($id);
            $this->loadvideo_con();
            echo '<script>alert("Successfully Deleted");</script>'  ; 

    } 

}
 
 
