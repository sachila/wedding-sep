<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class galarydb extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
   // add new images
  public function add_attachment($jobspec,$id,$path){       
             
              $this->db->set('file',$jobspec);          
              $this->db->set('n_id',$id);
              $this->db->set('path',$path);
              $query = $this->db->insert('files');
             return;
    } 
    //remove_image
    public function remove_image($id){
         $this->db->where('id',$id);
         $this->db->delete('files');
        
    }
    //remove_video
    public function remove_video($id){
         $this->db->where('id',$id);
         $this->db->delete('videos');
        
    }
    //add new videos
    public function add_videos($jobspec,$id,$path,$cat,$des){       
             
              $this->db->set('file',$jobspec);          
              $this->db->set('n_id',$id);
              $this->db->set('path',$path);
              $this->db->set('cat',$cat);
              $this->db->set('des',$des);
              
              $query = $this->db->insert('videos');
             return;
    } 
    //load image by image id 
      function get_images($id) {
        $this->db->select("id,file,n_id");
        $this->db->from('files');
        $this->db->where('n_id',$id);
        $query = $this->db->get();
        return $result = $query->result();
    }
    //load video by username 
      function get_videos($id) {
        $this->db->select("id,file,n_id");
        $this->db->from('videos');
        $this->db->where('n_id',$id);
        $query = $this->db->get();
        return $result = $query->result();
    }
    //load video by id 
      function get_videosbyid($id) {
        $this->db->select("*");
        $this->db->from('videos');
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $result = $query->result();
    }
  
}