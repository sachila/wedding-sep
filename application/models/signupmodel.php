<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class signupmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function customersignup() {
        $da = 'cust';
        $data = array(
            'name' => $this->input->post('custname'),
            'email' => $this->input->post('custemail'),
            'password' => $this->input->post('firstpass'),
            'mob' => $this->input->post('mob'),
            'type' => $da,
        );
        $this->db->insert('custsp', $data);
    }

    public function spsignup($name,$email,$password,$mob,$sppack,$web,$fb,$tw,$youtube,$spty) {
        $da = 'sp';
        $currentdate = date("Y-m-d");
        $dates = date_create($currentdate);
        date_add($dates, date_interval_create_from_date_string("365 days"));

        $emaildate = date_format($dates, "Y-m-d");

        
        $this->db->set('name', $name);
        $this->db->set('email', $email);
        $this->db->set('password', $password);
        $this->db->set('mob', $mob);
        $this->db->set('type', $da);
        $this->db->set('datefirst', $currentdate);
        $this->db->set('datesecond', $emaildate);
        $this->db->set('sppack', $sppack);
        $this->db->set('sptype', $spty);
        if ($web != NULL)
            $this->db->set('web', $web);
        if ($fb != NULL)
            $this->db->set('fb', $fb);
        if ($tw != NULL)
            $this->db->set('tw', $tw);
        if ($ytb != NULL)
            $this->db->set('youtube', $youtube);

        $query = $this->db->insert('custsp');
    }

    public function spsignuppay($mail) {
        $id = $this->getid($mail);
        $pd = 'yes';
        $data = array('paid' => $pd);
        $this->db->where('id', $id);
        $this->db->update('custsp', $data);
        return;
    }

    public function getid($email) {
        $this->db->select('id');
        $this->db->from('custsp');
        $this->db->where('email', $email);
        return $this->db->get()->row()->id;
    }

}

?>
