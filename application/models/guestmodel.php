<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class guestmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function getcommnts(){
        $this->db->select("*");
        $this->db->from('guestbk');
        $query = $this->db->get();
        return $query->result();
    
    }
    public function insertcmmnt(){
        $date=date("Y-m-d");
        $data=array(
            'name'=>$this->$input->post('guestname'),
            'email'=>$this->$input->post('guestmail'),
            'findus'=>$this->$input->post('findus'),
            'comment'=>$this->$input->post('cmnt'),
            'date'=>$date,
            'country'=>$this->$input->post('cntry'),
        );
        $this->db->insert('guestbk',$data);
        
    }
}