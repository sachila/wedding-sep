<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class package extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    //load additional details in user profile 
    public function user_additional_info($ptype,$user,$des,$total){
        $this->db->set('ptype',$ptype);
        $this->db->set('user',$user);
        $this->db->set('des',$des);
        $this->db->set('total',$total);
        $this->db->insert("user_addition");
        return;
         
    }
    //load additional details by id
    public function get_additional_detail_byid($id){
        $this->db->select('title,price');
        $this->db->from('additional_details');
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $result = $query->result();
        
    }

    //add new additional details
    public function add_addtional_details($file_name,$path,$title,$description,$price){
        $this->db->set('filename',$file_name);
        $this->db->set('path',$path);
        $this->db->set('title',$title);
        $this->db->set('description',$description);
        $this->db->set('price',$price);
        $this->db->insert('additional_details');
        return;
    }
    //update user new details to db
    public function edit_userpackage($id,$ptype, $person, $date, $user, $systemUser, $startdate,$enddate,$description,$location,$Charge){
        //get all data and put them to array 
        $data = array(
               'ptype' => $ptype,                
               'person' => $person,
               'Date' => $date,
               'user' => $user,
               'paymenttype' => $systemUser,                
               'startdate' => $startdate,
               'enddate' => $enddate,
               'description' => $description,
               'location' => $location,                
               'Charge' => $Charge                                              
              );
            
        $this->db->where('id', $id);
        $this->db->update('user_packages', $data);
          return;
    }
    //add new user package 
    public function add_userpackage($ptype, $person, $date, $user, $systemUser, $startdate,$enddate,$description,$location,$Charge){
       
            $this->db->set('ptype',$ptype); 
            $this->db->set('person',$person);
            $this->db->set('Date',$date); 
            $this->db->set('user',$user); 
            $this->db->set('paymenttype',$systemUser);
            $this->db->set('startdate',$startdate);
            $this->db->set('enddate',$enddate);
            $this->db->set('description',$description);
            $this->db->set('location',$location);
            $this->db->set('Charge',$Charge);
            $query = $this->db->insert('user_packages');            
            return;
    }
        // add new meal package 
        public function add_package($ptype, $soups, $appetizers, $salads, $meat, $fish,$vegetable,$rice,$person,$date,$Charge,$desserts){
          
            $this->db->set('ptype',$ptype);
            $this->db->set('soups',$soups);
            $this->db->set('appetizers',$appetizers);
            $this->db->set('salads',$salads);
            $this->db->set('meat',$meat);
            $this->db->set('fish',$fish);
            $this->db->set('vegetable',$vegetable);
            $this->db->set('rice',$rice);
            $this->db->set('person',$person);
            $this->db->set('Date',$date);
            $this->db->set('Charge',$Charge);
            $this->db->set('desserts',$desserts);
            $query = $this->db->insert('new_packages');            
                 
            return;
    } 
    //get package by id
    function get_packageid(){
        $this->db->select("id");
        $this->db->from('user_packages');
        $query = $this->db->get();
        return $result = $query->row();
    }
    //get meal package data    
    function get_packages() {
        $this->db->select("id,ptype,soups,appetizers,salads,meat,fish,vegetable,rice,person,Date,Charge,desserts");
        $this->db->from('new_packages');
        $query = $this->db->get();
        return $result = $query->result();
    }
  
    //load exsistion packages         
    function exsisting_packages() {
        $this->db->select("id,ptype,person,Date,user,paymenttype,startdate,enddate,description,location,Charge");
        $this->db->from('user_packages');
        $query = $this->db->get();
        return $result = $query->result();
    }
    //get all additional data that user selected
    function get_user_addition(){
        $this->db->select("*");
        $this->db->from('user_addition');
        $query = $this->db->get();
        return $additional = $query->result();
   
    }
    //load all addition details 
    function get_additional_details() {
        $this->db->select("id,filename,path,title,description,price");
        $this->db->from('additional_details');          
        $query = $this->db->get();
        return $result = $query->result();
    }
     //get current selected package by id 
    function get_exsisting_packages_byid($id) {
        $this->db->select("id,ptype,person,Date,user,paymenttype,startdate,enddate,description,location,Charge");
        $this->db->from('user_packages');
        $this->db->where('id',$id);  
        $query = $this->db->get();
        return $result = $query->result();
    }
    //load package by id
    function get_exsisting_packages_byname($id) {
        $this->db->select("id,ptype,person,Date,user,paymenttype,startdate,enddate,description,location,Charge");
        $this->db->from('user_packages');
        $this->db->where('ptype',$id);  
        $query = $this->db->get();
        return $result = $query->result();
    }
    //get meal package according to id 
    function get_package_byid($id){
        $this->db->select("*");
        $this->db->from('new_packages');       
        $this->db->where('id',$id);        
        $query  =   $this->db->get();        
        return $query->result();
    }
    //update meal packege 
      function update_package($id,$ptype, $soups, $appetizers, $salads, $meat, $fish,$vegetable,$rice,$person,$date,$Charge,$desserts){
         //get all data and put them to array 
          $data = array(
               'ptype' => $ptype,
               'soups' => $soups,
               'appetizers' => $appetizers,
               'salads' => $salads,
               'meat' => $meat,
               'fish' => $fish,
               'vegetable' => $vegetable,
               'rice' => $rice,
               'person' => $person,
               'date' => $date,
               'Charge' => $Charge,
               'desserts' => $desserts
            );
        $this->db->where('id', $id);
        $this->db->update('new_packages', $data);
          return;
    }
      //deleta meal package 
      function delete_category($checked_messages){

                  $this->db->where('id',$checked_messages);
                  $this->db->delete('new_packages'); 
      }
      //delete additional details
      function delete_addional_details($id){
                  $this->db->where('id',$id);
                  $this->db->delete('additional_details');          
      }
      //delete user category
      function delete_user_category($checked_messages){

                  $this->db->where('id',$checked_messages);
                  $this->db->delete('user_packages'); 
      }
      //search additionsl details by title
      function search_type_detail($title){
         //get all data and put them to array 
          $data = array(
          'title' => $title             
          );
          
        $this->db->select("*");
        $this->db->from('additional_details');
        
             if ($title != NULL ) {
                  $this->db->where('title', $title);
                  $query = $this->db->get();
                  return $query->result();
                 }
             else{     
 
              $this->db->where($data);
              $query  =   $this->db->get(); 
              return $query->result();
             }
    
      }
      //search meal packages by name        
      function search_type($ptype){
          //get all data and put them to array 
          $data = array(
          'ptype' => $ptype             
          );          
        $this->db->select("id,ptype,soups,appetizers,salads,meat,fish,vegetable,rice,person,Date,Charge,desserts");
        $this->db->from('new_packages');
        
             if ($ptype != NULL ) {
                  $this->db->where('ptype', $ptype);
                  $query = $this->db->get();
                  return $query->result();
                 }
             else{     
 
              $this->db->where($data);
              $query  =   $this->db->get(); 
              return $query->result();
             }
    
        }
   }
?>
