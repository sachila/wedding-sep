<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class my_controller extends CI_Controller { 
    
    // redirect to image load page
    public function setting_video($id){
         $this->load->model('galarydb');
         $this->load->helper('url'); 
         //load video by id 
         try {
            $data['result'] = $this->galarydb->get_videosbyid($id);
            $this->load->view('navbar');
            $this->load->view('view_one_video',$data);
            $this->load->view('footer');
         } catch (Exception $exc) {
             echo $exc->getTraceAsString();
         }
         }
    //load user addition page 
    public function user_additional_details_index($meal,$user){
        //get meal details to addition page
        $data['meal'][] = (object) array('meal' => $meal , 'user' => $user );
        $this->load->model('package');
        $this->load->helper('url'); 
        //load all addition details 
        try {
            $data['result'] = $this->package->get_additional_details();
            $this->load->view('navbar');
            $this->load->view('user_additional_details',$data);
            $this->load->view('footer');  
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        
    }
     //add new additional details to db by admin
    public function new_additional_details(){        
        $this->load->view('navbar');
        $this->load->view('new_additional_details');
        $this->load->view('footer');
        
    }
    // load additional details page 
    public function addtional_details_index(){    
        $this->load->model('package');
        $this->load->helper('url');  
        //get all additional details 
        try {
            $data['result'] = $this->package->get_additional_details();  
            $this->load->view('navbar');
            $this->load->view('addition_details',$data);
            $this->load->view('footer');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

         
    }   
    
    // save edited user packages 
    public function edit_user_package(){
        //validate user enter details 
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>'); 
        $this->form_validation->set_rules('systemUser', 'Payment type', 'required');
        $this->form_validation->set_rules('startdate', 'Start Date', 'required');	
        $this->form_validation->set_rules('enddate', 'End Date', 'required');	
        $this->form_validation->set_rules('description', 'Description', 'required');	
        $this->form_validation->set_rules('location', 'Location', 'required');	
        $id = $this->input->post('packageid');
 
     if ($this->form_validation->run() == FALSE)
		{
                //show if there is invelid details 
               $this->user_edit_package($id);
		}
		else{
                
                 //get all form details to variables 
                 $ptype = $this->input->post('ptype');           
                 $location = $this->input->post('location');
                 $description = $this->input->post('description');
                 $enddate = $this->input->post('enddate');
                 $startdate = $this->input->post('startdate');
                 $systemUser = $this->input->post('systemUser');
                 $user = $this->input->post('user');
                 $Charge = $this->input->post('Charge');
                 $date = $this->input->post('Date');
                 $person = $this->input->post('person');
                 $this->load->model('package');
                 
                 try {
                 //update user new details to db
                 $this->package->edit_userpackage($id,$ptype, $person, $date, $user, $systemUser, $startdate,$enddate,$description,$location,$Charge);
                 echo '<script>alert("Successfully Updated");</script>';
                 $this->exsisting_package();
                     
                 } catch (Exception $exc) {
                     echo $exc->getTraceAsString();
                     echo '<script>alert("Update Fails!!");</script>';
                 }



       
              
      }
    }
    //edit user selectes meal package
    public function user_edit_package($id){
        $this->load->model('package');
        $this->load->helper('url'); 
        try {
            //get current selected package by id 
            $data['result'] = $this->package->get_exsisting_packages_byid($id);
            $this->load->view('navbar');
            $this->load->view('user_edit_package',$data);
            $this->load->view('footer'); 
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    
    }
    //validatio that allow user to add only one meal package to one profile 
    public function user_add_package(){
        $this->load->model('package');
        $this->load->helper('url'); 
        try {
            $query = $this->db->query("SELECT * FROM user_packages");       
            //if meal package already added 
            if( $query->num_rows() > 0){
                 echo '<script>alert("Package Is Already Added");</script>';
                 $this->exsisting_package();
            }
            //if meal package not added to profile 
            else if($query->num_rows() == 0){
                $this->all_package();
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
  
   }
   //get user selected package 
   public function exsisting_package(){
        $this->load->model('package');
        $this->load->helper('url'); 
        try {            
            //check user already add meal package to profile
            $data['result'] = $this->package->exsisting_packages();
            //check user already add additional details to profile
            $data['additional'] = $this->package->get_user_addition();
            $query = $this->db->query("SELECT * FROM user_packages");  
            //if added
            if($query-> num_rows() == 1){
                for($i=0;$i<count($data['result']);$i++){
                //get package id to pid variable
                $pid = $data['result'][$i]->id;            
            }
            //get package by package id 
            $data['package'] = $this->package->get_package_byid($pid);
           // $data['newpackage'] = (object) array_merge((array) $data['result'], (array)  $data['package']);
            $this->load->view('navbar');
            $this->load->view('user_exsisting_packages',$data);
            $this->load->view('footer'); 

            }
            //if not added 
            else{
                //load all packages 
                $this->all_package();
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    //view all meal packages 
    public function all_package(){
        $this->load->model('package');
        $this->load->helper('url'); 
        try {
            //get all meal details 
            $data['result'] = $this->package->get_packages();
            $this->load->view('navbar');
            $this->load->view('all_packages',$data);
            $this->load->view('footer');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

         
    }
    //add new meal package to user account
    public function user_new_package($id){ 
        
        try {
            //check user is already add a meal package to the profile        
            $query = $this->db->query("SELECT * FROM user_packages");    
            //if meal package not added 

            if($query-> num_rows() == 0){              
            $this->load->model('package');
            $this->load->helper('url'); 
            //get meal package according to id 
            $data['result'] = $this->package->get_package_byid($id);
            $this->load->view('navbar');
            $this->load->view('user_new_package',$data);
            $this->load->view('footer');                     
            }
            //if package added to profile 
            else{
                  echo '<script>alert("Package Is Already Added To Your Account");</script>';
                  //load all packages 
                  $this->all_package();
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    //add new meal package 
    public function package_index(){     
        $this->load->model('package');
        $this->load->helper('url'); 
        //get meal package data
        try {
            $data['result'] = $this->package->get_packages();
            $this->load->view('navbar');
            $this->load->view('add_packages',$data);
            $this->load->view('footer');   
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }      
    }
    // add new meal package
    public function new_package(){
        $this->load->view('navbar');
        $this->load->view('new_package'); 
        $this->load->view('footer');   
   }   
   // redirect to image load page
   public function uploadimages_con() {
        $id = "SAS";
        $this->load->model('galarydb');
        $this->load->helper('url'); 
        try {
            $data['result'] = $this->galarydb->get_images($id);        
            $this->load->helper('url');
            $this->load->view('navbar');
            $this->load->view('loadimages',$data);   
            $this->load->view('footer');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }        
}
    //redirect to video load page
    public function loadvideo_con(){
        $id = "SAS";
        $this->load->model('galarydb');
        $this->load->helper('url'); 
        try {
             $data['result'] = $this->galarydb->get_videos($id);        
            $this->load->helper('url');
            $this->load->view('navbar');
            $this->load->view('loadvideos',$data);   
            $this->load->view('footer');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
}
    //search meal packages
    function search_location()
    {
        $data = array();
        $ptype = $this->input->post('ptype');
        $this->load->helper('url');
        $this->load->model('package');
        try {
            //search meal packages by name
            $data['result'] = $this->package->search_type($ptype);
            $this->load->view('navbar');
            $this->load->view('add_packages',$data);
            $this->load->view('footer'); 
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
   
    }
    //search additional details
    function addition_search_new(){
        $data = array();
        $title = $this->input->post('title');
        $this->load->helper('url');
        $this->load->model('package');
        try {
            //search additionsl details by title
            $data['result'] = $this->package->search_type_detail($title);
            $this->load->view('navbar');
            $this->load->view('addition_details',$data);
            $this->load->view('footer'); 
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

      
    }
}
?>
