<div class="footer">
   	<div class="container">
   	 <div class="footer_top">
   	   <h3>Subscribe to our newsletter</h3>
   	   <form>
		<span>
			<i><img src="<?php echo base_url('images/mail.png')?>" alt=""></i>
		    <input type="email" placeholder="Keyword, name, date, ..." style="width:325px">
		    <label class="btn1 btn2 btn-2 btn-2g"> <input name="submit" type="submit" id="submit" value="Subscribe"> </label>
		    <div class="clearfix"> </div>
		</span>			 	    
	   </form>
	  </div>
	  <div class="footer_grids">
	     <div class="footer-grid">
			<h4>  </h4>
			<ul class="list1">
				<li><a href="<?php echo site_url('welcome/contactus') ?>">Contact</a></li>
				<li><a href="<?php echo site_url('welcome/termsandcond') ?>">Terms and Conditions</a></li>
				<li><a href="<?php echo site_url('welcome/guestbook') ?>">Guest Book</a></li>
				<li><a href="<?php echo site_url('welcome/ourmission') ?>">Our Mission</a></li>
				
			</ul>
		  </div>
              <div class="footer-grid">
			
		  </div>

		  
		  <div class="footer-grid last_grid">
			<h4>Follow Us</h4>
			<ul class="footer_social wow fadeInLeft" data-wow-delay="0.4s">
			  <li><a href=""> <i class="fb"> </i> </a></li>
			  <li><a href=""><i class="tw"> </i> </a></li>
			  <li><a href=""><i class="google"> </i> </a></li>
			  <li><a href=""><i class="u_tube"> </i> </a></li>
		 	</ul>
		 	<div class="copy wow fadeInRight" data-wow-delay="0.4s">
             
	        </div>
		  </div>
		  <div class="clearfix"> </div>
	   </div>
      </div>
   </div>