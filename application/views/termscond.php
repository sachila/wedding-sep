<body>

    <div class="bannertwoin">
   
        <div class="container_wrap">
            <span class="titles"><h1 style="color: #000000;"><b>Terms Conditions &amp; Policies</b></h1></span>
		</p>
		<p class="titlesSub">March 1st 2015</p>
		<div>
				<div align="justify">We are committed to a policy of keeping information that we collect from you, our client in strict confidence. The information we collect whether personal or financial will be used only for the purpose of planning, your wedding and or supplying other services as required. The information will be shared with our service providers when required. When you use our site you imply that you agree with our privacy policy. When you place an order or pay for the services you agree to our Terms Conditions and Policies and business practices. We will not use the information for any commercial purposes other than to market our own products and inform you of any events, enhancements and changes to our business profile. We will not provide this information to third parties. On completing the coordination of a wedding we may have the pleasure of publishing a selected photograph on our website and/or display photographs by way of albums or enlargements unless if you have opted not to have them published or displayed.</div>
				<div>&nbsp;</div>
				<div align="justify">By using <a href="http://www.mywedding.com/">http://www.mywedding.com/</a> you agree to be bound by the following terms and conditions, as well as the terms and conditions of our service providers. These terms and conditions may change from time to time with or without notice and you agree to such changes when they occur or are posted on our web site.</div>
				<div>
						<strong>
						</strong>&nbsp;</div>
				<div align="justify">myWedding Incorporated provides a web site for the main purpose of planning weddings in Sri Lanka for those who wish to have their weddings in Sri Lanka. We also provide several related services for the entertainment, promotion of marriage and travel in the Island. All services are provided in Sri Lanka with a few exceptions. We have enlisted a team of professionals with whom we work closely. The services and planning are coordinated by the Head Office in the United States of America, through its family of professionals, coordinators and offices in Sri Lanka.</div>
				<div>&nbsp;</div>
				<div>
						<strong>
								<span class="titlesSub">
										<strong>Online Plan</strong>
								</span>
						</strong>
				</div>
				<div>
						<strong>
						</strong>&nbsp;</div>
				<div align="justify">We provide an online plan for the express purpose of helping couples to draw a reasonable budget for their wedding plans. While we try to give the most current prices based on averages, charged by prominent professionals in the fields published in the online plan, it should be expressly understood that individual prices could change according to your ultimate design and the specific Professionals you choose. There are no obligations attached to the use of this FREE planning device. Neither the Corporation nor the client is bound by any estimates provided. However the Corporation will use your plan selections as a guide in itâ€™s planning, when you retain us.&nbsp;&nbsp;</div>
				<div>&nbsp;</div>
				<div align="justify">When you submit a plan with the initiation fee paid we will commence working with you to coordinate a wedding plan for you. We encourage you to familiarize yourself with all published material on the site to have a good understanding of our sophisticated methods used. The initiation fee will allow you to view planning guides that will be of immense use in the planning process. It should be understood that these planning guides are for your exclusive use and should not be copied or shared with others outside of the wedding group. The guides do not constitute any commitment but should be used as a guide for planning purposes only. You will commit only to a final plan. The initiation fee is deductible from the final coordination fee but is not refundable. We prefer to work with one â€œdecision makerâ€ in planning weddings and may refuse planning with multiple decision makers. We encourage the couple to discuss their plans with family and friends and the coordinator with final decisions made by the appointed decision maker. In the planning process the couple and the bridal party are expected to corporate in the planning cessions to make the coordination a success. We will not be responsible for any delays caused by the arrival of the members of the wedding party as scheduled by us. Long delays may cause a domino effect and we may have to curtail activities to catch up on time schedules. While we discourage this type of behavior and take all steps to prevent it, we cannot be held responsible for individual non-corporation.&nbsp;&nbsp;&nbsp;</div>
				<div>&nbsp;</div>
				<div>
						<strong>
								<span class="titlesSub">
										<strong>Wedding Packages (Sri Lankans)</strong>
								</span>
						</strong>
				</div>
				<div>
						<strong>
						</strong>&nbsp;</div>
				<div align="justify">We provide a range of wedding packages, for Sri Lankans pre designed with minimum flexibility in order to provide clients the best available resources at a minimal cost. Due to changes in cost, the cost shown may vary without notice. We try to have the cost of the packages at the most current rates but if they have changed we will inform you of the actual cost at time of your order. Once you agree and place the order, your cost will not change unless if you decide to make changes.</div>
				<div>To book these packages you are required to pay a certain percentage up front so that we may make the advance payments and book the professionals. Please read the refund policies.</div>
				<div>
						<strong>
						</strong>&nbsp;</div>
				<div>
						<strong>
								<span class="titlesSub">
										<strong>Wedding Packages (Non Sri Lankans)</strong>
								</span>
						</strong>
				</div>
				<div>&nbsp;</div>
				<div align="justify">We provide <em><u>one stop</u></em> Wedding Packages for non Sri Lankans. These destination weddings in Sri Lanka, can be combined with travel plans. Since we work with all resorts, hotels, villas etc. the packages will be tailor made to suit the couple and the entourage. We will request payment up front in order to make the bookings and refunds will be guided by our refund policies stated elsewhere.&nbsp;</div>
				<div>&nbsp;</div>
				<div>
						<strong>
								<span class="titlesSub">
										<strong>Affiliations: </strong>
								</span>
						</strong>
				</div>
				<div>
						<strong>
						</strong>&nbsp;</div>
				<div align="justify">We also work with affiliate companies, especially Ethelton Investments (PVT) Ltd, a company formed in Sri Lanka, and we may join with others to bring you more services.&nbsp; We may use the affiliate companies to provide us services if needed. If WedInSriLanka uses the services of an affiliate company to provide you with any services, then WedInSriLanka terms and conditions and privacy policies will prevail for such services provided; as if they were provided by WedInSriLanka.&nbsp;</div>
				<div>&nbsp;</div>
				<div>
						<strong>
								<span class="titlesSub">
										<strong>Booking of Professionals/Hotels/Churches/Kovils/Resorts</strong>
								</span>
						</strong>
				</div>
				<div>
						<strong>
						</strong>&nbsp;</div>
				<div align="justify">You agree that planning on the online plan does not guarantee the services you selected nor the choices of Professionals etc. you desire, nor the availability of Hotels etc at the times and dates you have planned. These will be available only if booked in advance and coordinated. You will be required to make the advance payments necessary to retain the professionals &amp; Hotels etc. to WedInSriLanka and we will in turn make the payments and confirm the bookings. Since Hotels, Professionals, Churches etc go on a first come first served basis you will corporate in acting fast and making the commitments without delay. It should be understood, that the Corporation is unable to guarantee the availability, of all your choices for the date and time requested. However Hotels/Churches/Kovils have to be booked concurrently to avoid the non-availability of one as against the other.&nbsp;&nbsp;&nbsp;&nbsp;</div>
				<div>&nbsp;</div>
				<div align="justify">When you submit a plan and pay the initiation fee and select the professionals, hotels etc. you agree to pay the required advances immediately in order to confirm the bookings on your behalf. You also agree to pay the balance payments directly to the professionals or pay through our company bank accounts in Sri Lanka or in the US or through the web payment counter at appropriate currency exchange rates. These may include taxes, transfer fees etc. Our planning fee and service provider fees in full should be paid at least one week prior to the wedding/reception. We take full responsibility of coordinating the wedding as agreed and provide you with at least one coordinator and if necessary one or two helpers to plan the wedding. Please see the general information for further details. Our desire is to plan your wedding as perfectly as possible. You may also read our coordination efforts, elsewhere on the website.</div>
				<div>
						<strong>
								<font size="5">
								</font>
						</strong>&nbsp;</div>
				<div>
						<strong>
								<span class="titlesSub">
										<strong>
												<a class="FCK__AnchorC" name="proposals">Marriage Proposals</a>
										</strong>
								</span>
						</strong>
				</div>
				<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
		</div>
		<div align="justify">WedInSriLanka provides ex patriots of Sri Lankan origin and residents of Sri Lanka and others from all over the world a venue to have marriage proposals placed on our web site, free of charge unless we change this policy at a later date. These advertisements are placed at your own risk. We will take every precaution to keep your names and identity confidential and keep your incoming or outgoing email in strict confidence. We will use only an advertisement number as a reference to the reader and replies automatically forwarded using only such number. Replies will be directed to you through the email address supplied when posting the advertisement; in a safe mail delivery system. The email address, phone numbers, names and other confidential data that you supply will not be published <em>unless if you write them in the body of the advertisement.</em> If you give such information in the body of the advertisement, you will be fully responsible for its usage by all others who visit our web site.</div>
		<div align="justify">We strongly urge those using this service to use it for the purpose it is meant and any misuse could lead to penalties in law when applied by the respective authorities. It should be understood that your advertisements and replies are transmitted through the World Wide Web where people are known to intercept, trace or copy any publications. Unpleasant, foul or indecent language should not be used in the advertisements or in the replies. WedInSriLanka may not be aware of&nbsp;the content of replies that are transmitted automatically nor for replies sent by individuals whether&nbsp;you have supplied the reference in the body of the advertisement or not. We will not be responsible for any lost or misdirected email nor will be responsible for the security of the email addresses used by our clients. However secure our web site is, there is no guarantee that a hacker could penetrate the web site or the senders or recipients address etc. While we require the advertisers name, address, telephone numbers etc. before an advertisement could be placed, we do not guarantee that genuine advertises have placed the advertisements. The contents of the advertisement or the responses to them are the sole responsibility of the respective parties and WedInSriLanka is not responsible for such content. The parties, warrant that they are fully aware of any laws that may govern the correspondence and that they are abiding by such laws. It should be understood that the responsibility to observe the laws lies with the clients that use the service and at no time with WedInSriLanka. The selection of partners is the sole responsibility of the clients and WedInSriLanka is not responsible for any misunderstanding, selection of incompatible partners, break up of marriages etc. If residents of any other country place the proposals such proposals should be if ultimately result in marriage, legally binding, in the countries it originate. We have the right at our discretion to refuse any application without giving any reason for such refusal. We may also edit a proposal before publishing for the sole purpose of enhancing its quality. The advertisers and those who respond to them will be required to open a free account before they use this facility. If we find any improper use of this facility we have the right to block such users permanently or temporarily until we are satisfied of the users true intent. The proposals will be published within 48 hours and will remain for 60 days or a maximum of 90 days unless if you request us to delete it. On such a request we will verify the request using the original email address/address etc. If a marriage does take place on any of the advertisements placed at this site we will be happy to arrange the wedding if performed in Sri Lanka. The Corporation, nor the client has any obligation to retain WedInSriLanka, nor is WedInSriLanka obliged to plan such a wedding. Please also note that as an added precaution we have established that clients who place advertisements and those who reply them through our site as stated earlier should establish an account by loging in to do so.&nbsp;<br><br><span class="titlesSub"><strong>Account Creation and Log in facility <br><br></strong></span></div>
		<div align="justify">
				<span class="titlesSub">
						<strong>
								<font color="#808080">While we want all our clients to access and brows the website and use its facilities we do require serious users to obtain an account with us in order to be a part of our clientele who wish to participate in using the site to save wedding plans, packages, marriage proposal and replies and guest book. These services are free and we encourage our clients to save their plans and communicate with us by email or telephone. Wedding Planning starts only after a client registers with a deductible retaining fee.&nbsp;We reserve the right to block any user permanently or temporarily if we find evidence of misuse or other improper usage of these facilities.</font>
						</strong>
				</span>
		</div>
		<div>&nbsp;</div>
		<div>
				<strong>
						<span class="titlesSub">
								<strong>Refund Policy</strong>
						</span>
				</strong>
		</div>
		<div>
				<strong>
				</strong>&nbsp;</div>
		<div align="justify">When you make an authorization for us to make a booking for a Hotel/Church/Kovil/Resort or any other services, you will pay us the required advance payments immediately and we will make the payment to the services provider within a reasonable time if the services are available for the time &amp; Date. All service providers have their own refund policy. Most often these providers will not grant refunds. Some will have partial refund policies and others will refund if the cancellation is done within a reasonable time. If a marriage is postponed for valid reasons some providers may allow the advance or a part of it to be applied for a new date, if such date and time is available and booked instead. Some will allow a full refund if proof is provided of the death or major illness of either the groom or bride. WedInSriLanka is bound by these, individual Booking and Refund Policies. If a wedding is cancelled or postponed or the venue is changed we will not be held liable for any refunds that the provider is not liable to make under their refund policies. Wherever a refund is available we have the right to cover any expenses we may have incurred up to the time we were notified of the cancellation or change. We also reserve the right to transfer a booking cancelled by you if we find another party that can be accommodated for the vacancy created. In this event we will make appropriate refunds as available. All refunds will be made at the equivalent US currency and exchange rates that were available at the time of payment/refund date at our discretion. Any Bank Charges or exchange rate changes at the time of refund will be your responsibility.&nbsp;</div>
		<div>&nbsp;</div>
		<div>
				<strong>
						<span class="titlesSub">
								<strong>Copyrights: Trademarks</strong>
						</span>
				</strong>
		</div>
		<div>&nbsp;</div>
		<div align="justify">The contents of this web site, its design, individually and as a whole is the sole property of WedInSriLanka Incorporated (USA). The trade marks whether registered or not and contents whether text, database, logos, pictures and the software developed for the web site are the exclusive property of the Corporation and will be protected by the United States Copyright Laws and US State laws and the international Copyright Laws. The modification, reproduction, display, distribution or transmission of the contents of this web site is strictly prohibited. Any infringement of these copyrights will be prosecuted with the full force of the applicable laws.&nbsp;</div>
		<div>&nbsp;</div>
		<div>
				<strong>
						<span class="titlesSub">
								<strong>Disclaimer:</strong>
						</span>&nbsp;&nbsp;&nbsp;</strong>
				<span>&nbsp;</span>
		</div>
		<p align="justify">While we screen our service providers carefully, we will not be held liable for any lapse in the performance of any service provider. While we anticipate providing the best services possible, it is understood and agreed that under certain circumstances we may not be able to guarantee the performance of the services. This will include but not limited to the following: strikes, labor disputes, natural disasters, curfews, act of war, act of terrorism, flight cancellations, deaths, sicknesses etc. may render it impossible for either the Corporation, its affiliates and providers to perform its services. In such an event we will make every effort to obtain alternate arrangements wherever possible and to provide refunds to the extent that such refunds can be obtained from the provider. We will not be liable for any damage sustained by the client due to such unforeseen circumstance.</p>
		<p>&nbsp;</p>
</span><br>
     
            <div class="clearfix"></div>
        </div>
    </div>
   
    

</body>
</html>