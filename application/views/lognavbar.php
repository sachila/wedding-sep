<html>
    <head>
        <title>Wedding Planner</title>
        <?php echo link_tag('css/bootstrap.css')?>

        
       <?php echo link_tag('css/style.css')?>

        <!-- Custom Theme files -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
      
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        
 <?php echo link_tag('http://fonts.googleapis.com/css?family=Montserrat:400,700')?>      
        
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/login.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<!--Animation-->
<script src="js/wow.min.js"></script>
    
<script>
	new WOW().init();
</script>

    </head>
    <div class="header">
        <div class="col-sm-8 header-left">
            <div class="logo">
                <a href="index.html"><img src=" <?php echo base_url('images/log1.png')?> "/></a>					 </div>
            <div class="menu">
                <a class="toggleMenu" href="#"><img src="<?php echo base_url('images/nav.png')?>" alt="" /></a>
                <ul class="nav" id="nav">
                    <li class="active"><a href="<?php echo site_url('welcome/index') ?>">Home</a></li>
                    <li><a href="<?php echo site_url('welcome/wedplannervs') ?>">Wedding Planner</a></li>
                    <li><a href="education.html">Service Providers</a></li>
                    <li><a href="entertain.html">Packages</a></li>
                    <li><a href="<?php echo site_url('welcome/contactus') ?>">Contact Us</a></li>
                    <div class="clearfix"></div>
                </ul>
               <script type="text/javascript" src="js/responsive-nav.js"></script>

            </div>	
            <!-- start search-->
            <div class="search-box">
                <div id="sb-search" class="sb-search">
                    <form>
                        <input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
                        <input class="sb-search-submit" type="submit" value="">
                        <span class="sb-icon-search"> </span>
                    </form>
                </div>
            </div>
            <!----search-scripts---->
              <script src="js/classie.js"></script><script src="js/uisearch.js"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>

           
            <!----//search-scripts---->						
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-4 header_right">
            <ul class="nav" id="nav">
                <li>
                    <div id="loginContainer" style="height:71px;"><a href="#" id="loginButton"><img src="<?php echo base_url('images/login.png')?>"><span>LogOut</span></a>
                    <br/>
                        <img src="<?php echo base_url('images/sett.png')?>"><select>
                         
                            <option style="background-color: #e94e38;color: whitesmoke; ">My Account</option>
                            <option style="background-color: #e94e38;color: whitesmoke; ">Saved Orders</option>
                            <option style="background-color: #e94e38;color: whitesmoke; ">C</option>
                        
                        </select>
                    </div>
                </li>
                
                <li>
                       
                </li>   
            </ul>

        </div>

        <div class="clearfix"></div>
    </div>

    <div class="clearfix"></div>
