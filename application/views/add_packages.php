<!--viscustwpmain-->
<head>
    <?php echo link_tag('Assets/jquery-ui.min.css')?>  
   
    <script type="text/javascript" src="<?php echo base_url('Assets');?>/jquery-1.11.1.min.js"></script>

<script type="text/javascript" src="<?php echo base_url('Assets');?>/jquery-ui.min.js"></script>
</head>   
<body>
   <div class="bannertwoin">
                
   <div class="container">
        
  
      <div style="margin-left: -675px;">
      <?php echo form_open('packages/search_category')?>    
      
         <input id="ptype" type="text" name="ptype" style="height:30px;width:400px"/> 
         <button style="margin-right: 10px;" type="submit" class="btn btn-default">Search</button>
      
         <?php echo form_close()?>
      </div>
       
       <?php echo form_open('packages/remove_category')?>    
      <br>
      <br>
      <div style="margin-left: -900px;"> 
         <button style="margin-right: 10px;" type="button" class="btn btn-success"  onclick="location.href='<?php echo site_url('packages/newpackage');?>'">Add</button>
         <button style="margin-right: 10px;" type="submit" class="btn btn-danger" onclick="return validate_form()">Delete</button>
         <button type="button" class="btn btn-info" onclick="location.href='<?php  echo site_url('packages/additional_details');?>'">Additional</button>
      </div>
      <br>
      <table class="table table-striped">
         <thead>
            <tr>
               <th>Id</th>
               <th>Package Type</th>
               <th>Per Person</th>
               <th>Date</th>
            </tr>
         </thead>
         <tbody>
            <?php foreach($result  as $result): ?>     
            <tr>
               <td>
                  <input type="checkbox" name="msg[]" value="<?php echo $result->id; ?>"/>
               </td>
               <td>
                  <a href="<?php echo base_url('index.php/packages/updateid')."/".$result->id; ?>">
                  <?php echo $result->ptype; ?>
                  </a>
               </td>
               <td><?php echo $result->person; ?>
               </td>
               <td><?php echo $result->Date; ?>
            </tr>
            <?php endforeach; ?>    
         </tbody>
      </table>
  
   <?php echo form_close()?>
        </div>
       
            <script type="text/javascript">
        
        $(function() {
    var availableTags = <?php include('package_autocomplete.php'); ?>;
    $("#ptype").autocomplete({
        source: availableTags,
        autoFocus:true
    });
});
  
        
        
        </script>
        
        
   <script> 
      function validate_form()
      {
      valid = true;
      
      if($('input[type=checkbox]:checked').length == 0)
      {
      alert ( "ERROR! Please select at least one checkbox" );
      valid = false;
      }else if ($('input[type=checkbox]:checked').length != 0){
      
      job=confirm("Are you sure to delete permanently?");
      if(job!=true)
      {
       valid = false;
      }
      else{
      window.location.href = "<?php echo site_url('packages/remove_category'); ?>";
      }
      }
      
      return valid;
      }
           
   </script>
</body>
</html>