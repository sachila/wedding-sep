<!--viscustwpmain-->
<head>
    <?php echo link_tag('Assets/jquery-ui.min.css')?>  
   
    <script type="text/javascript" src="<?php echo base_url('Assets');?>/jquery-1.11.1.min.js"></script>

<script type="text/javascript" src="<?php echo base_url('Assets');?>/jquery-ui.min.js"></script>
</head>   
<body>
   <div class="bannertwoin">
                
   <div class="container">
        
  
      <div style="margin-left: -675px;">
      <?php echo form_open('packages/addition_search')?>    
      
         <input id="title" type="text" name="title" style="height:30px;width:400px"/> 
         <button style="margin-right: 10px;" type="submit" class="btn btn-default">Search</button>
      
         <?php echo form_close()?>
      </div>
       
       <?php echo form_open('packages/remove_additional_details')?>    
      <br>
      <br>
      <div style="margin-left: -1000px;"> 
         <button style="margin-right: 10px;" type="button" class="btn btn-success"  onclick="location.href='<?php echo site_url('packages/add_additional_details');?>'">Add</button>
         <button style="margin-right: 10px;" type="submit" class="btn btn-danger" onclick="return validate_form()">Delete</button>
       </div>
      <br>
      <table class="table table-striped">
         <thead>
            <tr>
               <th>Id</th>
               <th>Title</th>
               <th>Price</th>
               <th>File Name</th>
            </tr>
         </thead>
         <tbody>
            <?php foreach($result  as $result): ?>     
            <tr>
               <td>
                  <input type="checkbox" name="msg[]" value="<?php echo $result->id; ?>"/>
               </td>
               <td>
                  <!--<a href="<!?php echo base_url('index.php/packages/updateid')."/".$result->id; ?>">-->
                  <?php echo $result->title; ?>
                  <!--</a>-->
               </td>
               <td><?php echo $result->price; ?>
               </td>
               <td><?php echo $result->filename; ?>
            </tr>
            <?php endforeach; ?>    
         </tbody>
      </table>
  
   <?php echo form_close()?>
        </div>
       
            <script type="text/javascript">
        
        $(function() {
    var availableTags = <?php include('addition_autocomplete.php'); ?>;
    $("#title").autocomplete({
        source: availableTags,
        autoFocus:true
    });
});
  
        
        
        </script>
        
        
   <script> 
      function validate_form()
      {
      valid = true;
      
      if($('input[type=checkbox]:checked').length == 0)
      {
      alert ( "ERROR! Please select at least one checkbox" );
      valid = false;
      }else if ($('input[type=checkbox]:checked').length != 0){
      
      job=confirm("Are you sure to delete permanently?");
      if(job!=true)
      {
       valid = false;
      }
      else{
      window.location.href = "<?php echo site_url('packages/remove_additional_details'); ?>";
      }
      }
      
      return valid;
      }
           
   </script>
</body>
</html>