<!--visitorplanner-->

<body>

    <div class="bannertwoins">
        <form>
        <div class="container_wrap">

            <h1>Plan your wedding....</h1>
            <table align="center">
                
                <tr>
                    <td><h3>Hotel</h3></td>

               <td>
   <input type="text" placeholder="Keyword, name, date, ..." style="height:30px;width:400px" required>
               </td>
                </tr>
           <tr>
            <td>Guests</td>
            <td>
            <select class="dropdown" style="height:30px;width:100px" value="Select" required>
            <option value="0" selected>0</option>	
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="150">150</option>
            <option value="200">200</option>
            <option value="250">250</option>
            <option value="300">300</option>
            <option value="350">350</option>
            <option value="400">400</option>
            <option value="450">450</option>
            <option value="500">500</option>
            </select></td>
                
            </tr>
                  <tr><td style="border-top: black thin solid"><h3>Dressing &amp; Makeup</h3></td>
</tr>
<tr>
    <td>Bride (Wedding):</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl2_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl2$chkSelect" >
        <br/>
        
    </td>
</tr>

<tr>
    <td>Bride (going away):</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl3_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl3$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Bride (homecoming):</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl4_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl4$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Bride's Maids:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl5$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl5_drpNumeric" style="width:60px;">
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Flower Girls:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl6$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl6_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Other:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl7$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl7_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>

	</select>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Dressmaking</h3></td>
</tr>
<tr>
    <td>Bride (Wedding):</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl9_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl9$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Bride (going away):</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl10_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl10$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Bride (homecoming):</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl11_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl11$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Bride's Maids:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl12$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl12_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Flower Girls:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl13$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl13_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Other:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl14$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl14_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>

	</select>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Bouquets/Flower Arrangements</h3></td>
</tr>
<tr>
    <td>Bridal Bouquet:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl16_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl16$chkSelect" required>
        <br/>
        
    </td>
</tr>

<tr>
    <td>Bride's Headdress:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl17_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl17$chkSelect" required>
        <br/>
        
    </td>
</tr>

<tr>
    <td>Goingaway Bouquet &amp; Headdress:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl18_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl18$chkSelect" required>
        <br/>
        
    </td>
</tr>

<tr>
    <td>Homecoming Bouquet &amp; Headdress:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl19_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl19$chkSelect" required>
        <br/>
        
    </td>
</tr>

<tr>
    <td>Groom's &amp; Bestman's Boutonniere:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl20$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl20_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Bride's Maids Bouquet &amp; Headdress:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl21$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl21_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Flower Girl's Bouquet &amp; Headdress:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl22$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl22_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Pageboy's Boutonniere:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl23$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl23_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>

	</select>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Church Decoration</h3></td>
</tr>
<tr>
    <td>Alter:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl25_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl25$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Church Entrance:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl26_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl26$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Main Church Pews:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl27_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl27$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Side Church Pews:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl28_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl28$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Wedding Reception</h3></td>
</tr>
<tr>
    <td>Reception Tables:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl30$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl30_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
		<option value="31">31</option>
		<option value="32">32</option>
		<option value="33">33</option>
		<option value="34">34</option>
		<option value="35">35</option>
		<option value="36">36</option>
		<option value="37">37</option>
		<option value="38">38</option>
		<option value="39">39</option>
		<option value="40">40</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Entrance:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl31_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl31$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Bridal Settee:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl32_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl32$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Poruwa:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl33_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl33$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Bridal &amp; Party Car Streamers:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl34_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl34$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Traditional Oil Lamp Decoration:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl35_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl35$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Flowers at Hotelrooms:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl36$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl36_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Hotel Deco for Homecoming (High Stands):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl37$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl37_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>

	</select>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Poruwa Ceremony</h3></td>
</tr>
<tr>
    <td>Supply of Required Material:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl39_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl39$chkSelect">
        
        <br/>
    </td>
</tr>

<tr>
    <td>Master of Ceremonies:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl40_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl40$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Four Jayamangala Girls:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl41_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl41$chkSelect">
        <br/>
        
    </td>
</tr>


    <tr>
    <td>Two Drummers/Two Kandyan Dancers/One Hakgedi:</td>
    <td><input id="ctl00_ContentPlaceHolder1_plannerControl_optTdTkdOh" type="radio" name="ctl00$ContentPlaceHolder1$plannerControl$DancersOption" value="optTdTkdOh"></td>
    </tr>
    <tr>
        <td>Four Drummers/Four Dancers/One Hakgedi:</td>
        <td><input id="ctl00_ContentPlaceHolder1_plannerControl_optFdFdOh" type="radio" name="ctl00$ContentPlaceHolder1$plannerControl$DancersOption" value="optFdFdOh"></td>
    </tr>
    
<tr>
    <td>Two Additional Dancers:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl95_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl95$chkSelect">
        <br/>
        
    </td>
</tr>

    
    
    
    
<tr>
    <td style="border-top: black thin solid"><h3>Car Service</h3></td>
</tr>
<tr>
    <td>Car for Bridal Party:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl43$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl43_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Car for Groom's Party:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl44$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl44_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Car for Other Family Members:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl45$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl45_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>

	</select>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Cake Structures/Pieces</h3></td>
</tr>
<tr>
    <td>Cake Structure:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl46$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl46_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Cake Pieces:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl47$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl47_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="50">50</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option value="200">200</option>
		<option value="250">250</option>
		<option value="300">300</option>
		<option value="350">350</option>
		<option value="400">400</option>
		<option value="450">450</option>
		<option value="500">500</option>
		<option value="550">550</option>
		<option value="600">600</option>
		<option value="650">650</option>
		<option value="700">700</option>
		<option value="750">750</option>
		<option value="800">800</option>
		<option value="850">850</option>
		<option value="900">900</option>
		<option value="950">950</option>
		<option value="1000">1000</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Cake Boxes/Bags:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl48$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl48_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="50">50</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option value="200">200</option>
		<option value="250">250</option>
		<option value="300">300</option>
		<option value="350">350</option>
		<option value="400">400</option>
		<option value="450">450</option>
		<option value="500">500</option>
		<option value="550">550</option>
		<option value="600">600</option>
		<option value="650">650</option>
		<option value="700">700</option>
		<option value="750">750</option>
		<option value="800">800</option>
		<option value="850">850</option>
		<option value="900">900</option>
		<option value="950">950</option>
		<option value="1000">1000</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Chocolate Fountain:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl98_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl98$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Photographer</h3></td>
</tr>
<tr>
    <td>50-page Album (Durogard laminated):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl50$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl50_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Extra Pages:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl51$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl51_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Studio Photographs (4 copies):</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl52_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl52$chkSelect">
        
        <br/>
    </td>
</tr>

<tr>
    <td>Black &amp; White Copy:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl53$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl53_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Homecoming (40-page Album):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl54$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl54_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Video of Wedding:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl55_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl55$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Video of Homecoming:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl56_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl56$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Postcard Photos:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl57$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl57_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="50">50</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option value="200">200</option>
		<option value="250">250</option>
		<option value="300">300</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Thanking Cards:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl94$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl94_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="50">50</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option value="200">200</option>
		<option value="250">250</option>
		<option value="300">300</option>

	</select>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Cards</h3></td>
</tr>
<tr>
    <td>Invitation Cards (Reception/Church):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl59$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl59_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="100">100</option>
		<option value="200">200</option>
		<option value="300">300</option>
		<option value="400">400</option>
		<option value="500">500</option>
		<option value="600">600</option>
		<option value="700">700</option>
		<option value="800">800</option>
		<option value="900">900</option>
		<option value="1000">1000</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Invitation Cards (Homecoming):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl60$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl60_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="125">125</option>
		<option value="150">150</option>
		<option value="175">175</option>
		<option value="200">200</option>
		<option value="225">225</option>
		<option value="250">250</option>
		<option value="275">275</option>
		<option value="300">300</option>
		<option value="325">325</option>
		<option value="350">350</option>
		<option value="375">375</option>
		<option value="400">400</option>
		<option value="425">425</option>
		<option value="450">450</option>
		<option value="475">475</option>
		<option value="500">500</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Thank You Cards:</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl61$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl61_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="50">50</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option value="200">200</option>
		<option value="250">250</option>
		<option value="300">300</option>
		<option value="350">350</option>
		<option value="400">400</option>
		<option value="450">450</option>
		<option value="500">500</option>
		<option value="550">550</option>
		<option value="600">600</option>
		<option value="650">650</option>
		<option value="700">700</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Introduction of Wedding Party (Folded Sheets):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl62$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl62_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="50">50</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option value="200">200</option>
		<option value="250">250</option>
		<option value="300">300</option>

	</select>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Jewellery</h3></td>
</tr>
<tr>
    <td>Bride's Engagement Ring:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl64_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl64$chkSelect">
        
        <br/>
    </td>
</tr>

<tr>
    <td>Bride's Necklace:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl65_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl65$chkSelect">
        
        <br/>
    </td>
</tr>

<tr>
    <td>Groom's Engagement Ring:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl66_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl66$chkSelect">
        
        <br/>
    </td>
</tr>

<tr>
    <td>Bride's Wedding Band:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl67_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl67$chkSelect">
        
        <br/>
    </td>
</tr>

<tr>
    <td>Groom's Wedding Band:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl68_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl68$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Wines &amp; Liquor </h3></td>
</tr>
<tr>
    <td>Champagne for Toast only (per liter):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl70$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl70_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Champagne (per liter):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl71$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl71_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Red wine (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl72$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl72_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>White wine (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl73$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl73_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Whiskey (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl74$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl74_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Vodka (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl75$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl75_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Gin (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl76$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl76_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Scotch (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl77$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl77_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Brandy (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl78$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl78_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Bourbon (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl79$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl79_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Sherry (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl80$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl80_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Martini (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl81$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl81_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Vermouth (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl82$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl82_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Beer (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl83$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl83_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Coffee Liquor (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl84$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl84_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Chases (per liter):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl85$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl85_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td>Sparkling wine (per bottle):</td>
    <td>
        
        <select name="ctl00$ContentPlaceHolder1$plannerControl$wppControl86$drpNumeric" id="ctl00_ContentPlaceHolder1_plannerControl_wppControl86_drpNumeric" style="width:60px;" required>
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>

	</select>
        
    </td>
</tr>

<tr>
    <td style="border-top: black thin solid"><h3>Music</h3></td>
</tr>
<tr>
    <td>Band/Music (Wedding):</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl88_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl88$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Band/Music (Homecoming):</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl89_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl89$chkSelect">
        
        <br/>
    </td>
</tr>

<tr>
    <td>DJ:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl90_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl90$chkSelect">
        
        <br/>
    </td>
</tr>

<tr>
    <td>Church Choir:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl91_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl91$chkSelect">
        <br/>
        
    </td>
</tr>

<tr>
    <td>Soloist:</td>
    <td>
        <input id="ctl00_ContentPlaceHolder1_plannerControl_wppControl92_chkSelect" type="checkbox" name="ctl00$ContentPlaceHolder1$plannerControl$wppControl92$chkSelect">
        <br/>
        
    </td>
</tr>

    <tr>
        <td style="border-top: black thin solid"><h3>Honeymoon Resorts</h3></td> 
<td>
   <input type="text" placeholder="Keyword, name, date, ..." style="height:30px;width:400px" required >
               </td>
    </tr>
    <tr>
        <td></td>
        <td><div class="contact_btn">
                    <label class="btn1 btn-2 btn-2g"><input name="pro" type="button" id="pro" value="Proceed"></label>
                </div></td>
    </tr>
    
    </table>
        </div>
                
            
                
            </form>        		
            <div class="clearfix"></div>
        </div>
 
    


</body>
</html>